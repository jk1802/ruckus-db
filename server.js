const express = require('express')
const axios = require('axios')
const cors = require('cors')
const bodyParser = require('body-parser')
require('dotenv').config()

const ZOMATO_API_KEY = process.env.ZOMATO_API_KEY

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

app.get('/', (req, res) => {
  res.send('Ruckus backend server up and running.')
})

app.get('/api', async (req, res) => {
  try {
    const response = await axios.get(
      `https://developers.zomato.com/api/v2.1/collections?city_id=280&apikey=${ZOMATO_API_KEY}`
    )
    res.send(response.data)
  } catch (e) {
    console.error(e)
  }
})

app.post('/collections', async (req, res) => {
  try {
    const { collectionId } = await req.body
    const response = await axios.post(
      `https://developers.zomato.com/api/v2.1/search?collection_id=${collectionId}&apikey=${ZOMATO_API_KEY}`
    )
    res.send(response.data)
  } catch (e) {
    console.error(e)
  }
})

app.post('/restaurant', async (req, res) => {
  try {
    const { id } = await req.body
    const response = await axios.post(
      `https://developers.zomato.com/api/v2.1/restaurant?res_id=${id}&apikey=${ZOMATO_API_KEY}`
    )
    res.send(response.data)
  } catch (e) {
    console.error(e)
  }
})

const PORT = process.env.PORT || 3001
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`)
})
